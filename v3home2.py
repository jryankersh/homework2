#HW #2 Pseudocode
#
# ---------------------------------------------------- #
# File: v3home2.py
# ---------------------------------------------------- #
# Author(s): your_name(s), with BitBucket handle (name)
# [you may add confluence or github link]
# ---------------------------------------------------- #
# Plaftorm:    Unix
# Environment: Python          2.7.8
# Libaries:    numpy           1.9.0
#              matplotlib     u1.4.0
#       	   scipy          0.14.0
#              sympy            N/A
#              OpenCV          2.4.9
#       	   SciKits Image  0.10.1
# ---------------------------------------------------- #
# Description:
'''<docstring: put here the description of your code>'''
# <include name of other modules used/imported>
# <include name of other modules used/imported>
# ---------------------------------------------------- #

import scipy
import numpy as np
import matplotlib.pyplot as plt
import matplotlib.animation as animation
import scipy.ndimage as ndimage
import scipy.misc as misc
import random
import cv
import cv2
from glob import glob
from array import array
from numpy import sin, cos, pi, array


def GenerateVertices():
    # create vertex coords as a list [x,y] to make them easily accesible
    #place first vertex on y axis
    vert1 = [128]
    vert1.append(random.randint(64, 128 )) # start at 128 b/c x and y "axes" in middle
                                           # of the 256 by 256 area
                                           # Also, only allow vertex to be half way
                                           # between axis and 256 area edge by limiting
                                           # range between 0 + (128/2) = 64
    #place second vertex in 3rd quadrant
    vert2 = []
    vert2.append(random.randint(64, 128))
    vert2.append(random.randint(128, 192))
    #place third vertex in 4th quadrant
    vert3 = []
    vert3.append(random.randint(128, 192))
    vert3.append(random.randint(128, 192))

    print('vert 1', vert1)
    print('vert 2', vert2)
    print('vert 3', vert3)
    
    return vert1,vert2,vert3

# could also create two lists - 1 of x's and 1 of y's, then zip those lists together
        #actually, not here since we want the vertices in certain quadrants


def plot_verts( vert1, vert2, vert3):
    im = np.zeros((256,256))
    im[ vert1[1], vert1[0] ] = 1   #arrays accessed in form of [y,x]
    im[ vert2[1], vert2[0] ] = 1
    im[ vert3[1], vert3[0] ] = 1
    plt.imshow(im, cmap=plt.cm.gray)
    plt.show()

# get centroid of triangle (for rotation axis)
#--------------------------------------------------------
#def get_centroid(vert1, vert2, vert3)
#    centroid_x = (vert1[0] + vert2[0] + vert3[0] ) / 3
#    centroid_y = (vert1[1] + vert2[1] + vert3[1] ) / 3
# 
#    print('centroid x is', centroid_x)
#    print('centroid y is', centroid_y)
#
#    return centroid_x, centroid_y
#--------------------------------------------------------

def IsInside(vert1,vert2,vert3):

    sigma = 2    #int(input('Enter Desired Gaussian Smoothness:'))
    im = np.zeros((256,256))
    list_vert_xs = [vert1[0],vert2[0],vert3[0]]  # this way is shorter, 
        #but does it change the original info or does it COPY the data
        # YES, it COPIES the data rather than changing the data from vert1,2,3[x,y]
                #i.e. rather than referencing same data as original vert lists
    list_vert_ys = [vert1[1],vert2[1],vert3[1]]

    #print('xs are: ', list_vert_xs )
    #print('ys are: ', list_vert_ys )

    max_x = np.max(list_vert_xs)        # begin by creating a limit box so the algorithm 
    min_x = np.min(list_vert_xs)        #   doesn't have to consider every point in the  
    max_y = np.max(list_vert_ys)        #   array when testing if the point is inside the
    min_y = np.min(list_vert_ys)        #   triangle, can just start at the
                                        #   max/min x and y coords
    #print('max x is: ', max_x)
    #print('max y is: ', max_y)

    for ii in range(min_x, max_x):      # dot_product test - WORKS!!!
        for jj in range(min_y, max_y):
            #im[jj,ii] = 1      
            # x = ii
            # x1 = vert1[0]
            # x2 = vert2[0]
            # x3 = vert3[0]
        
            #y = jj
            #y1 = vert1[1]
            #y2 = vert2[1]
            #y3 = vert3[1]
        
            dot1 = (vert2[1] - vert1[1])*(ii - vert1[0]) + (-vert2[0] + vert1[0])*(jj - 
            vert1[1])
            dot2 = (vert3[1] - vert2[1])*(ii - vert2[0]) + (-vert3[0] + vert2[0])*(jj -
            vert2[1])
            dot3 = (vert1[1] - vert3[1])*(ii - vert3[0]) + (-vert1[0] + vert3[0])*(jj - 
            vert3[1])
        
            if dot1 >= 0 :
                if dot2 >= 0 :
                    if dot3 >= 0 :
                        im[jj,ii] = 1
    
    # Dot Product Formulas 
    # from - http://totologic.blogspot.fr/2014/01/accurate-point-in-triangle-test.html  
    #dot1 = = (y2 - y1)*(x - x1) + (-x2 + x1)*(y - y1)
    #dot2 = = (y3 - y2)*(x - x2) + (-x3 + x2)*(y - y2)
    #dot3 = = (y1 - y3)*(x - x3) + (-x1 + x3)*(y - y3) 

    im = ndimage.gaussian_filter(im,sigma)      #change gaussian smoothness
    return im


#SPIN AND SAVE
def spin_and_save(im, prefix): 
    rot_degree = 3.6
    #prefix = 'rot_im'
    list_length = 100

    for ii in range(0, list_length):
        if ii == 0:
            im_rot = ndimage.rotate(im, 0, reshape = False)
            file_name = 'rot_im_00%s.png' %(ii)
            misc.imsave(file_name, im_rot) 
            #im_rot.tofile(file_name)
        elif ii < 10:
            im_rot = ndimage.rotate(im_rot, rot_degree, reshape = False)
            file_name = 'rot_im_00%s.png' %(ii)
            misc.imsave(file_name, im_rot) 
            #im_rot.tofile(file_name)
        elif ii >= 10 & ii < 101:
            im_rot = ndimage.rotate(im_rot, rot_degree, reshape = False)
            file_name = 'rot_im_0%s.png' %(ii)
            misc.imsave(file_name, im_rot) 
            #im_rot.tofile(file_name)
        else:
            im_rot = ndimage.rotate(im_rot, rot_degree, reshape = False)
            file_name = 'rot_im_%s.png' %(ii)
            misc.imsave(file_name, im_rot) 
    

#play animation of triangle spinning
def play_ani(filelist):                         # animate images into a movie
    ang_vel = 45    # = int(input('Enter Angular Velocity for Spinning:'))
    intervalz = (3.6/ang_vel)*1000              # change interval b/w images in animation
                                                #   to change angular velocity       
    fig = plt.figure()                         
    ims = []                                    # create empty list to load imagez 
    for ii in range (0,len(filelist)):          #   for the animation
        imagez = misc.imread(filelist[ii]).reshape(256,256)
        ploter = plt.imshow(imagez)
        ims.append([ploter])
    
    ani = animation.ArtistAnimation(fig, ims, interval = intervalz, 
    repeat=False) #interval in MILIseconds
    ani.save('mov1.mp4', fps=30)
    plt.show()


#trying stuff from notes - trying to find vertices w/ binary_opneing
#    different ways to open files
#         im3 = ndimage.imread('triz.png', flatten = True)
#         im3 = misc.imread('triz.png', True).astype(float)


def Binary_Vertices(image_filename):

    im3 = misc.imread(image_filename).reshape(256,256)
    struc2 = [[1,1,1],[1,1,1],[0,1,0]]       #THIS ONE IS THE BEST
    opened_image2 = ndimage.binary_opening(im3, structure = struc2, iterations=1)
    #closed_image2 = ndimage.binary_closing(im3, structure = struc2, iterations=1)
    closed_image = ndimage.binary_closing(im3)
    showz2= ndimage.binary_dilation(closed_image - opened_image2)

#   if sigma !=0:
#     struc3 = [[1,1,1],[1,1,1],[1,1,1]]
#     chek = ndimage.binary_erosion(im3, structure = struc3, iterations=2)
#     opened_image7 = ndimage.binary_opening(chek, structure = struc2, iterations=4)
#     #closed_image2 = ndimage.binary_closing(im3, structure = struc2, iterations=1)
#     closed_image7 = ndimage.binary_closing(chek)
#     showz7= ndimage.binary_dilation(closed_image7 - opened_image7)


    #plt.imshow(showz2, cmap=plt.cm.gray)  #BEST OF SHOWZZ
    # plt.axis('off')
    #plt.title('showz2')
    #plt.show()

    # Number 2 - Alternate way, using mask
    # mask2 = opened_image2
    # im4[mask2] = 0
    # plt.imshow(im4, cmap=plt.cm.gray)
    # plt.title('BEST')
    # plt.show()

    labeled_array, num_features = ndimage.label(showz2)
    
    return showz2, labeled_array, num_features


#Harris Corner - using scikit image
def HarrisCorner_method1(image_filename):               #shows verts on image and
    im7 = misc.imread(image_filename).reshape(256,256)  # returns vertex peak coords
    from skimage import data                            #  as an array
    from skimage.feature import corner_harris, corner_subpix, corner_peaks

    coords = corner_peaks(corner_harris(im7), min_distance=5)
    #coords_subpix = corner_subpix(im7, coords, window_size=6)

    fig, ax = plt.subplots()
    ax.imshow(im7, interpolation='nearest', cmap=plt.cm.gray)
    ax.plot(coords[:, 1], coords[:, 0], '.b', markersize=14)
    #ax.plot(coords_subpix[:, 1], coords_subpix[:, 0], '.r', markersize=10)
    ax.axis((0, 256, 256, 0))
    plt.show()

    print('the vertex coords are:', coords)
    return coords


# Harris Corner - using OpenCV                        # plots vertices, returns image
def HarrisCorner_method2(image_filename, show=0):       # BUT DOES NOT return coords
    kappa = .08#float(input('Enter kappa value (recomended .2 >=kappa >= 0.08):'))
    win_size = 2#int(input('Enter window size (recomended 2 >= win_size >= 6):'))
    img  = cv2.imread(image_filename)                
    
    gray = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)
    gray = np.float32(gray)

    dst = cv2.cornerHarris(gray,win_size,3,kappa)
    dst = cv2.dilate(dst,None)
    img[dst>0.01*dst.max()]=[0,0,255]
    if show == 1:
        cv2.imshow('Vertices',img)
    
    # increasing window size makes function more sensitive to vertices/intersection
    #     and results in larger marked areas
    #         Therefore, keep window size between 2 and 6
    # increasing kappa makes the function less sensitive to vertices/intersection
    #     and results in smaller marked/highlighted areas 
    #     when kappa values are low, the function is more sensitive to non-straight lines
    #     and will mark jagged areas of those lines as vertices
    #         Therefore, kappa values should be kept between .06 and .2
    
        print('Press 0 to exit.')
        if cv2.waitKey(0) & 0xff == 27:
            cv2.destroyAllWindows()
    
    
    return img


# main()
vert1, vert2, vert3 = GenerateVertices()
#plot_verts( vert1, vert2, vert3)
im = IsInside(vert1,vert2,vert3)

# Find files with correct prefix before opening files
prefix = 'rot_im'       #raw_input('Enter filename prefix for saving and loading images:')
spin_and_save(im,prefix)

files = '%s*.png' %prefix 
filelist = glob(files)
filelist.sort()

#play_ani(filelist)
coords = HarrisCorner_method1(filelist[0])

show = 1  #int(input('Show Plot of Harris Vertices?   Yes:1, No:0'))
marked_verts_im = HarrisCorner_method2(filelist[0], show)
grey_verts, labeled_array, num_features = Binary_Vertices(filelist[0])


plt.imshow(labeled_array)
plt.show()










cap = cv2.VideoCapture('mov1.mp4')
# params for ShiTomasi corner detection
feature_params = dict( maxCorners = 100,
                       qualityLevel = 0.3,
                       minDistance = 7,
                       blockSize = 7 )

# Parameters for lucas kanade optical flow
lk_params = dict( winSize  = (15,15),
                  maxLevel = 2,
                  criteria = (cv2.TERM_CRITERIA_EPS | cv2.TERM_CRITERIA_COUNT, 10, 0.03))

# Create some random colors
color = np.random.randint(0,255,(100,3))

# Take first frame and find corners in it
ret, old_frame = cap.read()
old_gray = cv2.cvtColor(old_frame, cv2.COLOR_BGR2GRAY)
p0 = cv2.goodFeaturesToTrack(old_gray, mask = None, **feature_params)

# Create a mask image for drawing purposes
mask = np.zeros_like(old_frame)

while(1):
    ret,frame = cap.read()
    frame_gray = cv2.cvtColor(frame, cv2.COLOR_BGR2GRAY)

    # calculate optical flow
    p1, st, err = cv2.calcOpticalFlowPyrLK(old_gray, frame_gray, p0, None, **lk_params)

    # Select good points
    good_new = p1[st==1]
    good_old = p0[st==1]

    # draw the tracks
    for i,(new,old) in enumerate(zip(good_new,good_old)):
        a,b = new.ravel()
        c,d = old.ravel()
        mask = cv2.line(mask, (a,b),(c,d), color[i].tolist(), 2)
        frame = cv2.circle(frame,(a,b),5,color[i].tolist(),-1)
    img = cv2.add(frame,mask)

    #cv2.imshow('frame',img)
    k = cv2.waitKey(30) & 0xff
    if k == 27:
        break

    # Now update the previous frame and previous points
    old_gray = frame_gray.copy()
    p0 = good_new.reshape(-1,1,2)

cv2.destroyAllWindows()
cap.release()



# Find Edges Binary - Works
'''
#this works, but I don't know why
# I took it from http://stackoverflow.com/questions/14110904/
                                    #numpy-binary-raster-image-to-polygon-transformation
edges = (ndimage.filters.maximum_filter(im, size=2) == ndimage.filters.minimum_filter(im,
 size=2))
plt.imshow(edges, interpolation='nearest')
plt.show()
'''

# Find Edges w/ Sobel Works
'''
# sobel - from http://scipy-lectures.github.io/advanced/image_processing/
sx = ndimage.sobel(im, axis=0, mode='constant')
sy = ndimage.sobel(im, axis=1, mode='constant')
sob = np.hypot(sx, sy)
plt.imshow(sob)
plt.show()
'''

































#HW #2 Pseudocode

# ---------------------------------------------------- #
# File: v2home2.py
# ---------------------------------------------------- #
# Author(s): your_name(s), with BitBucket handle (name)
# [you may add confluence or github link]
# ---------------------------------------------------- #
# Plaftorm:    Unix
# Environment: Python 2.7.7
# Libaries:    numpy 1.8.1
#              matplotlib 1.3.1
#       	   scipy 0.14.0
#              sympy 0.7.5
#       	   
# ---------------------------------------------------- #
# Description:
'''<docstring: put here the description of your code>'''
# <include name of other modules used/imported>
# <include name of other modules used/imported>
# ---------------------------------------------------- #




import scipy
import numpy as np
import matplotlib.pyplot as plt
import matplotlib.animation as animation
import scipy.ndimage as ndimage
import scipy.misc as misc
from glob import glob
from array import array
from numpy import sin, cos, pi, array

import random
import cv
import cv2





'''
#Data Generation
GenerateRandomTriangle(sigma=0)
    GenerateVertices()
    IsInside(point,vertices)
'''

def GenerateVertices():
    #want to create vertex coords as a list [x,y] to make them easily accesible
    #place first vertex on y axis
    vert1 = [128]
    vert1.append(random.randint(64, 128 )) # start at 128 b/c x and y "axes" in middle
                        # of the 256 by 256 area
                        # also, only allow vertex to be half way between
                        #axis and 256 area edge by limiting range
                        # between 0 + (128/2) = 64
    #place second vertex in 3rd quadrant
    vert2 = []
    vert2.append(random.randint(64, 128))
    vert2.append(random.randint(128, 192))
    #place third vertex in 4th quadrant
    vert3 = []
    vert3.append(random.randint(128, 192))
    vert3.append(random.randint(128, 192))

    print('vert 1', vert1)
    print('vert 2', vert2)
    print('vert 3', vert3)
    
    return vert1,vert2,vert3


# could also create two lists - 1 of x's and 1 of y's, then zip those lists together
        #actually, not here since we want the vertices in certain quadrants


def plot_verts( vert1, vert2, vert3):
    im = np.zeros((256,256))
    im[ vert1[1], vert1[0] ] = 1   #arrays accessed in form of [y,x]
    im[ vert2[1], vert2[0] ] = 1
    im[ vert3[1], vert3[0] ] = 1

#max triangle limit - just for now to check correctness
#im[ 64, 128 ] = 1
#im[ 192, 64 ] = 1
#im[ 192, 192 ] = 1



# get centroid of triangle (for rotation axis)
#--------------------------------------------------------
#
# centroid_x = (vert1[0] + vert2[0] + vert3[0] ) / 3
# centroid_y = (vert1[1] + vert2[1] + vert3[1] ) / 3
# 
# im[ centroid_y, centroid_x ] = 1
# 
# print('centroid x is', centroid_x)
# print('centroid y is', centroid_y)
#
#--------------------------------------------------------



#-------------------------------------------------------

def IsInside(vert1,vert2,vert3):

     # begin by create a limit box so the algorithm doesn't have to consider every 
        # point in the array when testing if the point is inside the triangle
        # can just start at the max/min x and y coords

    sigma = 0    #int(input('Enter Desired Gaussian Smoothness:'))
    im = np.zeros((256,256))
    list_vert_xs = [vert1[0],vert2[0],vert3[0]]  # this way is shorter, 
        #but does it change the original info or does it COPY the data
        # YES, it COPIES the data rather than changing the data from vert1,2,3[x,y]
                #i.e. rather than referencing same data as original vert lists
    list_vert_ys = [vert1[1],vert2[1],vert3[1]]

    print('xs are: ', list_vert_xs )
    print('ys are: ', list_vert_ys )

    max_x = np.max(list_vert_xs)
    min_x = np.min(list_vert_xs)
    max_y = np.max(list_vert_ys)
    min_y = np.min(list_vert_ys)

    print('max x is: ', max_x)
    print('max y is: ', max_y)


    #dot_product test - WORKS!!!
    for ii in range(min_x, max_x):
        for jj in range(min_y, max_y):
            #im[jj,ii] = 1      
            # x = ii
            # x1 = vert1[0]
            # x2 = vert2[0]
            # x3 = vert3[0]
        
            #y = jj
            #y1 = vert1[1]
            #y2 = vert2[1]
            #y3 = vert3[1]
        
            dot1 = (vert2[1] - vert1[1])*(ii - vert1[0]) + (-vert2[0] + vert1[0])*(jj - 
            vert1[1])
            dot2 = (vert3[1] - vert2[1])*(ii - vert2[0]) + (-vert3[0] + vert2[0])*(jj -
            vert2[1])
            dot3 = (vert1[1] - vert3[1])*(ii - vert3[0]) + (-vert1[0] + vert3[0])*(jj - 
            vert3[1])
        
            if dot1 >= 0 :
                if dot2 >= 0 :
                    if dot3 >= 0 :
                        im[jj,ii] = 1
    
    # Dot Product Formulas 
    # from - http://totologic.blogspot.fr/2014/01/accurate-point-in-triangle-test.html  
    #dot1 = = (y2 - y1)*(x - x1) + (-x2 + x1)*(y - y1)
    #dot2 = = (y3 - y2)*(x - x2) + (-x3 + x2)*(y - y2)
    #dot3 = = (y1 - y3)*(x - x3) + (-x1 + x3)*(y - y3) 

    im = ndimage.gaussian_filter(im,sigma)      #change gaussian smoothness
    return im


#SPIN AND SAVE
def spin_and_save(im): 
    rot_degree = 3.6
    prefix = 'rot_im'
    list_length = 100

    for ii in range(0, list_length):
        if ii == 0:
            im_rot = ndimage.rotate(im, 0, reshape = False)
            file_name = 'rot_im_00%s.png' %(ii)
            misc.imsave(file_name, im_rot) 
            #im_rot.tofile(file_name)
        elif ii < 10:
            im_rot = ndimage.rotate(im_rot, rot_degree, reshape = False)
            file_name = 'rot_im_00%s.png' %(ii)
            misc.imsave(file_name, im_rot) 
            #im_rot.tofile(file_name)
        elif ii >= 10 & ii < 101:
            im_rot = ndimage.rotate(im_rot, rot_degree, reshape = False)
            file_name = 'rot_im_0%s.png' %(ii)
            misc.imsave(file_name, im_rot) 
            #im_rot.tofile(file_name)
        else:
            im_rot = ndimage.rotate(im_rot, rot_degree, reshape = False)
            file_name = 'rot_im_%s.png' %(ii)
            misc.imsave(file_name, im_rot) #THESE ALL SAVE CORRECTLY, BUT DONT ALL LOAD
                                            #CORRECTELY
        #im_rot.tofile(file_name)
        #plt.imshow(im_rot)
        #plt.savefig(file_name)





#play animation of triangle spinning
def play_ani():
    
    # Find files with correct prefix before opening files
    prefix = 'rot_im'
    files = '%s*.png' %prefix 
    filelist = glob(files)
    filelist.sort()
    #print(filelist)

    # animate images into a movie
    ang_vel = 45    # = int(input('Enter Angular Velocity for Spinning:'))
    intervalz = (3.6/ang_vel)*1000   #change interval b/w images in animation to change 
                                     #angular velocity
    print('this is interval', intervalz)
    fig = plt.figure()
    ims = []      #create empty list to load imagez for the animation
    for ii in range (0,len(filelist)):
        #imagez = np.fromfile(filelist[ii]).reshape(256,256)
        imagez = misc.imread(filelist[ii]).reshape(256,256)
        ploter = plt.imshow(imagez)
        ims.append([ploter])
    
    ani = animation.ArtistAnimation(fig, ims, interval = intervalz, 
    repeat=False) #interval in MILIseconds
    ani.save('mov1.mp4', fps=30)
    plt.show()



# main()
vert1, vert2, vert3 = GenerateVertices()
im = IsInside(vert1,vert2,vert3)
#spin_and_save(im)
#play_ani()






#gauss filter on delta fuction
'''
blurry_im = ndimage.gaussian_filter(im,2)

print im
print blurry_im

plt.imshow(im, cmap=plt.cm.gray)
plt.axis('off')
plt.savefig('triz.png')

plt.figure(figsize=(10,5))
plt.subplot(121)
plt.imshow(im, cmap=plt.cm.gray)
#plt.axis('off')
plt.subplot(122)
plt.imshow(blurry_im)
# plt.axis('off')

plt.show()
'''


#trying stuff from notes - trying to find vertices w/ binary_opneing
#ALMOST ABLE TO FIND VERTICES USING BINARY_OPEN

#im3 = ndimage.imread('triz.png', flatten = True)
#im3 = misc.imread('triz.png', True).astype(float)

prefix = 'rot_im'
files = '%s*.png' %prefix 
filelist = glob(files)
filelist.sort()

im3 = misc.imread(filelist[0]).reshape(256,256)
im4 = misc.imread(filelist[0]).reshape(256,256)
im5 = misc.imread(filelist[0]).reshape(256,256)
im22 = misc.imread(filelist[0]).reshape(256,256)

#im3 = im3/np.max(im3) #need this to normalize to 1's and zeros
#---------------------------------------
#
#       WHAT PICURE ARE WE IMPORTING TO FIND VERTICES???
#               OUR PICTURE???
#               NEW PICTURE???
#
#---------------------------------------

# plt.imshow(im3, cmap=plt.cm.gray)
# plt.title('Trizz')
# plt.show()
# print('this is im3')
# print(im3)
# print('length of im 3', im3.__len__())
#print('size' , im3.size())

'''
im4 = np.zeros((600,600))       #HOW DO I RESIZE IMAGE 3 TO 256X256
for ii in range(0, 599):
    for jj in range(0, 599):
        if im3[jj,ii] >= 1:
            im4[jj,ii] = 1
        else:
            im4[jj,ii] = 0  
#im4 = im3 < 1

print(im4) # WHY IS IM4 BOOL?? WHY NOT 0 AND 1 
                #ALSO, WHY IS THE GREAYSCALE REVERSESED???
plt.imshow(im4, cmap=plt.cm.gray)
plt.title('Im4')
plt.show()
'''















'''
#first filtered - Doesn't work as well as second 2
struc = [[0,1,0],[1,1,1],[0,1,0]]
opened_image = ndimage.binary_opening(im3, structure = struc, iterations=2)
# plt.imshow(opened_image, cmap=plt.cm.gray)
# plt.title('Opened_Image')
# plt.show()
#print(opened_image)

struc2 = [[1,1,1],[1,1,1],[0,1,0]]       #THIS ONE IS THE BEST
opened_image2 = ndimage.binary_opening(im3, structure = struc2, iterations=2)

struc3 = [[1,1,1],[1,1,1],[1,1,1]]
opened_image3 = ndimage.binary_opening(im3, structure = struc3, iterations=2)

plt.figure(figsize=(10,5))
plt.subplot(131)
plt.imshow(opened_image, cmap=plt.cm.gray)
#plt.axis('off')
plt.subplot(132)
plt.imshow(opened_image2, cmap=plt.cm.gray)
#plt.axis('off')
plt.subplot(133)
plt.imshow(opened_image3, cmap=plt.cm.gray)
# plt.axis('off')
plt.title('Comparison')
plt.show()

        #original
mask = opened_image
#im[mask] = 0

im3[mask] = 0
plt.imshow(im3, cmap=plt.cm.gray)
plt.title('im3_Image')
plt.show()

        # Number 2
mask2 = opened_image2
#im[mask] = 0

im4[mask2] = 0             #THIS ONE IS THE BEST
plt.imshow(im4, cmap=plt.cm.gray)
plt.title('BEST')
plt.show()

        # Number 3
mask3 = opened_image3
#im[mask] = 0

im5[mask3] = 0
plt.imshow(im5, cmap=plt.cm.gray)
plt.title('im5_Image')
plt.show()



closing = ndimage.binary_closing(im22)
opening = ndimage.binary_opening(im22)
showz = ndimage.binary_dilation(closing - opening)
plt.imshow(showz, cmap=plt.cm.gray)
# plt.axis('off')
plt.title('Showz')
plt.show()


plt.figure(figsize=(10,5))
plt.subplot(131)
plt.imshow(showz, cmap=plt.cm.gray)
#plt.axis('off')
plt.subplot(132)
plt.imshow(im4, cmap=plt.cm.gray)
# plt.axis('off')
plt.subplot(133)
plt.imshow(im5, cmap=plt.cm.gray)
# plt.axis('off')
plt.title('Comparison')
plt.show()


labeled_array, num_features = ndimage.label(im4)
plt.imshow(labeled_array)
plt.title('label')
plt.show()
print('image labels:', num_features)
#scipy.ndimage.interpolation.rotate(input, angle in degrees, axes=(1, 0), 
#reshape=True, output=None, order=3, mode='constant', cval=0.0, prefilter=True

'''












# Find Edges Binary - Works
'''
#this works, but I don't know why
# I took it from http://stackoverflow.com/questions/14110904/
                                    #numpy-binary-raster-image-to-polygon-transformation
edges = (ndimage.filters.maximum_filter(im, size=2) == ndimage.filters.minimum_filter(im,
 size=2))
plt.imshow(edges, interpolation='nearest')
plt.show()
'''

# Find Edges w/ Sobel Works
'''
# sobel - from http://scipy-lectures.github.io/advanced/image_processing/
sx = ndimage.sobel(im, axis=0, mode='constant')
sy = ndimage.sobel(im, axis=1, mode='constant')
sob = np.hypot(sx, sy)
plt.imshow(sob)
plt.show()
'''



# Harris Corner Stuff 
#
#   MOSTLY WORKING
#
#    from - http://scikit-image.org/docs/dev/auto_examples/plot_corner.html
#   on theory - http://opencv-python-tutroals.readthedocs.org/en/latest/py_tutorials/
#                    py_feature2d/py_features_harris/py_features_harris.html
# 
# ---------------------------------------------
# 

#Harris Corner - using scikit image
def HarrisCorner_method1():                          #shows verts on image and
    im7 = misc.imread(filelist[0]).reshape(256,256)  # returns vertex peak coords
    from skimage import data                         #  as an array
    from skimage.feature import corner_harris, corner_subpix, corner_peaks

    coords = corner_peaks(corner_harris(im7), min_distance=1)
    coords_subpix = corner_subpix(im7, coords, window_size=5)

    fig, ax = plt.subplots()
    ax.imshow(im7, interpolation='nearest', cmap=plt.cm.gray)
    ax.plot(coords[:, 1], coords[:, 0], '.b', markersize=15)
    ax.plot(coords_subpix[:, 1], coords_subpix[:, 0], '.r', markersize=10)
    ax.axis((0, 256, 256, 0))
    plt.show()

    print('the vertex coords are:', coords)
    
    return coords


# Harris Corner - using OpenCV                   # plots vertices
def HarrisCorner_method2():                      # BUT DOES NOT return coords
    kappa = .08#float(input('Enter kappa value (recomended .2 >=kappa >= 0.08):'))
    win_size = 2#int(input('Enter window size (recomended 2 >= win_size >= 6):'))
    img  = cv2.imread(filelist[0])                
    img1 = cv2.imread(filelist[0])
    img2 = cv2.imread(filelist[0])
    img3 = cv2.imread(filelist[0])
    
    gray = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)
    gray = np.float32(gray)

    dst = cv2.cornerHarris(gray,2,3,kappa)
    dst = cv2.dilate(dst,None)
    img[dst>0.01*dst.max()]=[0,0,255]
    cv2.imshow('dst',img)

    dst1 = cv2.cornerHarris(gray,3,3,kappa)
    dst1 = cv2.dilate(dst1,None)
    img1[dst1>0.01*dst1.max()]=[0,0,255]
    cv2.imshow('dst1',img1)

    dst2 = cv2.cornerHarris(gray,5,3,kappa)
    dst2 = cv2.dilate(dst2,None)
    img2[dst2>0.01*dst2.max()]=[0,0,255]
    cv2.imshow('dst2',img2)
    
    dst3 = cv2.cornerHarris(gray,6,3,kappa)
    dst3 = cv2.dilate(dst3,None)
    img3[dst3>0.01*dst3.max()]=[0,0,255]
    cv2.imshow('dst3',img3)
    
    # increasing window size makes function more sensitive to vertices/intersection
    #     and results in larger marked areas
    #         Therefore, keep window size between 2 and 6
    # increasing kappa makes the function less sensitive to vertices/intersection
    #     and results in smaller marked/highlighted areas 
    #     when kappa values are low, the function is more sensitive to non-straight lines
    #     and will mark jagged areas of those lines as vertices
    #         Therefore, kappa values should be kept between .06 and .2
    


    print('Press 0 to exit.')
    if cv2.waitKey(0) & 0xff == 27:
        cv2.destroyAllWindows()



spin_and_save(im)
coords = HarrisCorner_method1()
HarrisCorner_method2()




# 
# def harris_corner_detection(img):
#         gray = cv2.cvtColor(img,cv2.COLOR_BGR2GRAY)
#         gray = np.float32(gray)
#         dst = cv2.cornerHarris(gray,2,3,0.04)
#         dst = cv2.dilate(dst,None)
#         img[dst>0.01*dst.max()]=[0,0,255]
#         return img



# 
# -----------------------------------------------




#Gaussian filter
'''
sigma = 1
triangle_image = ndimage.gaussian_filter(im, sigma)
plt.imshow(triangle_image)
plt.title("triangle_image")
plt.show()
'''

















# trying another way to find corners w/ binary
# NOT WORKING SO FAR
'''
im5 = misc.imread(filelist[0]).reshape(256,256)
plt.imshow(im5, cmap=plt.cm.gray)
plt.title('im5')
plt.show()

struc = [[0,1,0],[1,1,1],[0,1,0]]
opened_image2 = ndimage.binary_opening(im5,  iterations=5)
plt.imshow(opened_image2, cmap=plt.cm.gray)
plt.title('Opened_Image_2')
plt.show()
print(opened_image2)

opened_image3 = np.zeros((opened_image2.__len__(),opened_image2.__len__()))
for ii in range(0, (opened_image3.__len__() - 1)):
    for jj in range(0, (opened_image3.__len__() - 1)):
        if opened_image2[jj,ii] == False:
            opened_image3[jj,ii] = 0
        else:
            opened_image3[jj,ii] = 1

print('opened image 3', opened_image3)
mask = opened_image3
#im[mask] = 0
plt.imshow(opened_image3, cmap=plt.cm.gray)
plt.title('Masked_Image_3')
plt.show()


im5[mask] = 0
plt.imshow(im6, cmap=plt.cm.gray)
plt.title('im6_Image')
plt.show()
'''






# another way of trying to use a mask 
# rotation to remove corners - DOES NOT WORK
'''
im4 = misc.imread(filelist[0]).reshape(256,256)
plt.imshow(im4, cmap=plt.cm.gray)
plt.title('im4')
plt.show()

mask1 = ndimage.rotate(im4, -10, reshape = False)
mask2 = ndimage.rotate(im4, 10, reshape = False)

im4[mask1] = 0
plt.imshow(im4, cmap=plt.cm.gray)
plt.title('im4 w/ mask_1')
plt.show()

im4[mask2] = 0
plt.imshow(im4, cmap=plt.cm.gray)
plt.title('im4 w/ mask_1 and mask_2')
plt.show()
'''
































